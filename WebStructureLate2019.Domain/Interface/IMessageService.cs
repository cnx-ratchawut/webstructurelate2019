﻿using System;
namespace WebStructureLate2019.Domain.Interface
{
    public interface IMessageService
    {
        string Message(string message_code, params string[] args);
        string Message(Exception ex);
    }
}
