# Structure
```mermaid
graph LR
 DAL		-->	Web
 Domain 	-->	Web
 Service 	-->	Web
 DAL 		-->	Service
 Domain 	-->	Service
 Domain 	-->	DAL
 X((T)) --> DAL
```
 
> **Note:** ยังเขียนไม่เสร็จ
> 
## Packages
- Install-Package	Autofac	-Version 	4.9.4
- Install-Package	Autofac.WebApi2 	-Version 	4.3.1
- Install-Package	Autofac.WebApi2.Owin	-Version 	4.0.0
- Install-Package	EntityFramework	-Version 	6.3.0
- Install-Package	Microsoft.Owin	-Version 	4.0.1
- Install-Package	Microsoft.Owin.Host.SystemWeb	-Version 	4.0.1
- Install-Package	Autofac.Mvc5 	-Version 	4.0.2
- Install-Package	Autofac.Mvc5.Owin 	-Version 	4.0.1
- Install-Package	Microsoft.Owin.Cors	-Version 	4.0.1
- Install-Package	Microsoft.Owin.FileSystems	-Version 	4.0.1
- Install-Package	Microsoft.Owin.StaticFiles	-Version 	4.0.1
- Install-Package	Swashbuckle	-Version 	5.6.0