﻿using Microsoft.Owin;
using Newtonsoft.Json.Serialization;
using Owin;
using System.Web.Http;
using WebStructureLate2019.App_Start;

[assembly: OwinStartup(typeof(WebStructureLate2019.Startup))]
namespace WebStructureLate2019
{
    public class Startup
    {
        public static void Configuration(IAppBuilder app)
        {
            var config = CreateHttpConfiguration();
            AppSeed.RegisterAutoFac(config, app);
            AppSeed.RegisterSwagger(config);
            AppSeed.RegisterStaticFile(app);
        }
        public static HttpConfiguration CreateHttpConfiguration()
        {
            var httpConfiguration = new HttpConfiguration();
            WebApiConfig.Register(httpConfiguration); //reuse web api route config with old configuration
            httpConfiguration.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            httpConfiguration.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;
            return httpConfiguration;
        }
    }
}