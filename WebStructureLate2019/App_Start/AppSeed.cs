﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;
using Owin;
using Swashbuckle.Application;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using WebStructureLate2019.Domain.Database;
using WebStructureLate2019.Service.Implement;

namespace WebStructureLate2019.App_Start
{
    public static class AppSeed
    {
        public static void RegisterRepoAndService(ContainerBuilder builder)
        {
            var service = Assembly.GetAssembly(typeof(MessageService));
            //var dataAccess = Assembly.GetAssembly(typeof(TransactionRepository));
            builder.RegisterAssemblyTypes(service)
                .Where(t => t.Name.EndsWith("Service"))
                .InstancePerRequest()
                .AsImplementedInterfaces()
                .PropertiesAutowired();

            //builder.RegisterAssemblyTypes(dataAccess)
            // .Where(t => t.Name.EndsWith("Repository"))
            // .AsImplementedInterfaces().PropertiesAutowired()
            // .InstancePerHttpRequest();

            builder.RegisterType<AppEntities>().InstancePerRequest();
        }
        public static void RegisterAutoFac(HttpConfiguration config, IAppBuilder app)
        {
            var builder = new ContainerBuilder();
            builder.RegisterWebApiFilterProvider(config);
            RegisterRepoAndService(builder);
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly()).PropertiesAutowired();
            builder.RegisterControllers(Assembly.GetExecutingAssembly()).PropertiesAutowired();
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);  //Set the WebApi DependencyResolver
            config.DependencyResolver = GlobalConfiguration.Configuration.DependencyResolver;
            app.UseAutofacMiddleware(container);
            app.UseAutofacMvc();
            app.UseAutofacWebApi(config);
        }
        public static void RegisterSwagger(HttpConfiguration config)
        {
            config
            .EnableSwagger(c => c.SingleApiVersion("v1", "A title for your API"))
            .EnableSwaggerUi();
        }
        public static void RegisterStaticFile(IAppBuilder app)
        {
            string root = AppDomain.CurrentDomain.BaseDirectory;


#if DEBUG
            app.UseCors(CorsOptions.AllowAll);
#endif

            var physicalFileSystem = new PhysicalFileSystem(Path.Combine(root, "wwwroot"));
            var options = new FileServerOptions
            {
                RequestPath = PathString.Empty,
                EnableDefaultFiles = true,
                FileSystem = physicalFileSystem
            };
            options.StaticFileOptions.FileSystem = physicalFileSystem;
            options.StaticFileOptions.ServeUnknownFileTypes = false;
            app.UseFileServer(options);
        }
    }
}